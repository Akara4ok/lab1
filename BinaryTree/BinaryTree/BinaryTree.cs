﻿using BinaryTree.BinaryTreeEventArgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BinaryTree
{
    public class BinaryTree<T> : IEnumerable<T>, IReadOnlyCollection<T> where T : IComparable<T>
    {
        public Node<T> root { get; set; }

        public int Count { get; private set; }

        public event EventHandler<ItemAddedEventArgs<T>> ItemAddedEvent;
        public event EventHandler<ClearTreeEventArgs> ClearTreeEvent;

        public bool IsReadOnly => true;

        private Node<T> Add(Node<T> root, T value)
        {
            if (root is null)
            {
                root = new Node<T>(value);
                return root;
            }

            if (value.CompareTo(root.Value) >= 0)
            {
                root.Right = Add(root.Right, value);
            }
            else
            {
                root.Left = Add(root.Left, value);
            }

            return root;
        }

        public void Add(T value)
        {
            root = Add(root, value);
            Count++;
            ItemAddedEvent?.Invoke(this, new ItemAddedEventArgs<T>(value));
        }


        private Node<T> Find(Node<T> root, T value)
        {
            if (root is null)
            {
                return null;
            }

            if (value.CompareTo(root.Value) == 0)
            {
                return root;
            }

            if (value.CompareTo(root.Value) > 0)
            {
                return Find(root.Right, value);
            }
            else
            {
                return Find(root.Left, value);
            }
        }

        public Node<T> Find(T value)
        {
            var node = Find(root, value);
            if (node is null)
                throw new KeyNotFoundException();

            return node;
        }

        public bool Contains(T value)
        {
            return Find(root, value) != null;
        }

        private IEnumerable<T> PreOrder(Node<T> root)
        {
            if (root == null)
                yield break;

            yield return root.Value;

            if (root.Left != null)
                foreach (T item in PreOrder(root.Left))
                    yield return item;

            if (root.Right != null)
                foreach (T item in PreOrder(root.Right))
                    yield return item;
        }

        public IEnumerable<T> PreOrder() => PreOrder(root);

        private IEnumerable<T> PostOrder(Node<T> root)
        {
            if (root == null)
                yield break;

            if (root.Left != null)
                foreach (T item in PostOrder(root.Left))
                    yield return item;

            if (root.Right != null)
                foreach (T item in PostOrder(root.Right))
                    yield return item;

            yield return root.Value;
        }

        public IEnumerable<T> PostOrder() => PostOrder(root);

        private IEnumerable<T> InOrder(Node<T> root)
        {
            if (root == null)
                yield break;

            if (root.Left != null)
                foreach (T item in InOrder(root.Left))
                    yield return item;

            yield return root.Value;

            if (root.Right != null)
                foreach (T item in InOrder(root.Right))
                    yield return item;
        }

        public IEnumerable<T> InOrder() => InOrder(root);


        public IEnumerator<T> GetEnumerator()
        {
            return InOrder().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void Clear()
        {
            if (Count == 0)
                throw new InvalidOperationException("Tree is already empty");

            root = null;
            int previousCount = Count;
            Count = 0;

            ClearTreeEvent?.Invoke(this, new ClearTreeEventArgs(previousCount));
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (arrayIndex + Count > array.Length)
                throw new IndexOutOfRangeException();

            foreach (T item in InOrder())
            {
                array[arrayIndex] = item;
                arrayIndex++;
            }
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }
    }
}