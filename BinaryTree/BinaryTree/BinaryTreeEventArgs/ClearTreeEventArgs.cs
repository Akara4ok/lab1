﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.BinaryTreeEventArgs
{
    public class ClearTreeEventArgs : EventArgs
    {
        public int Count { get; init; }

        public ClearTreeEventArgs(int count)
        {
            Count = count;
        }
    }
}
