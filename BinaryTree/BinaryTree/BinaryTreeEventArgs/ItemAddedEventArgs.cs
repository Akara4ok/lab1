﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.BinaryTreeEventArgs
{
    public class ItemAddedEventArgs<T> : EventArgs
    {
        public T Value { get; init; }

        public ItemAddedEventArgs(T value)
        {
            Value = value;
        }
    }
}
