﻿using BinaryTree;
using System.Collections.Generic;
using System;
using System.Linq;
using Xunit;

namespace BinaryTreeUnitTests
{
    public class BinaryTreeEventTests
    {
        [Fact]
        public void ItemAdded()
        {
            BinaryTree<int> tree = new BinaryTree<int>();
            List<int> addedItems = new List<int>();
            tree.ItemAddedEvent += (_, args) => addedItems.Add(args.Value);

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            Assert.Equal(new[] { 3, 1, 4, 5, 0, 2 }, addedItems);
        }

        [Fact]
       public void ClearTree()
        {
            BinaryTree<int> tree = new BinaryTree<int>();
            List<int> clearedValues = new List<int>();
            tree.ClearTreeEvent += (_, args) => clearedValues.Add(args.Count);

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            tree.Clear();

            tree.Add(3);
            tree.Add(1);

            tree.Clear();

            Assert.Equal(new[] { 6, 2 }, clearedValues);
        }
    }
}
