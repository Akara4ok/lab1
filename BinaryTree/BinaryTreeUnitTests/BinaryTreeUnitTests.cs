using BinaryTree;
using System.Collections.Generic;
using System;
using System.Linq;
using Xunit;

namespace BinaryTreeUnitTests
{
    public class BinaryTreeUnitTests
    {
        [Fact]
        public void Add()
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            Assert.Equal(3, tree.root.Value);
            Assert.Equal(1, tree.root.Left.Value);
            Assert.Equal(4, tree.root.Right.Value);
            Assert.Equal(5, tree.root.Right.Right.Value);
            Assert.Equal(0, tree.root.Left.Left.Value);
            Assert.Equal(2, tree.root.Left.Right.Value);
        }

        [Fact]
        public void Contains()
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            Assert.True(tree.Contains(3));
            Assert.True(tree.Contains(0));
            Assert.False(tree.Contains(-3));
            Assert.False(tree.Contains(100));
        }

        [Fact]
        public void Find()
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            Assert.Equal(tree.root, tree.Find(3));
            Assert.Equal(tree.root.Left, tree.Find(1));
            Assert.Equal(tree.root.Left.Left, tree.Find(0));
            Assert.Equal(tree.root.Left.Right, tree.Find(2));
            Assert.Throws<KeyNotFoundException>(() => tree.Find(-1));
            Assert.Throws<KeyNotFoundException>(() => tree.Find(100));
        }

        [Fact]
        public void PreOrder()
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            Assert.Equal(new[] { 3, 1, 0, 2, 4, 5}, tree.PreOrder().ToArray());
        }

        [Fact]
        public void InOrder()
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            Assert.Equal(new[] { 0, 1, 2, 3, 4, 5 }, tree.ToArray());
        }

        [Fact]
        public void PostOrder()
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            Assert.Equal(new[] { 0, 2, 1, 5, 4, 3 }, tree.PostOrder().ToArray());
        }

        [Fact]
        public void Clear()
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            tree.Clear();
            Assert.Empty(tree);
            Assert.Null(tree.root);
            Assert.Throws<InvalidOperationException>(() => tree.Clear());
        }

        [Fact]
        public void Remove()
        {
            BinaryTree<int> tree = new BinaryTree<int>();

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            
            Assert.Throws<NotImplementedException>(() => tree.Remove(3));
        }
    }
}