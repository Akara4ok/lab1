﻿using BinaryTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryTree<int> tree = new BinaryTree<int>();
            tree.ItemAddedEvent += (_, args) => Console.WriteLine($"{args.Value} added");
            tree.ClearTreeEvent += (_, args) => Console.WriteLine($"{args.Count} elements deleted");

            tree.Add(3);
            tree.Add(1);
            tree.Add(4);
            tree.Add(5);
            tree.Add(0);
            tree.Add(2);

            Console.WriteLine(tree.Contains(3));
            foreach (var item in tree)
            {
                Console.WriteLine(item);
            }

            try
            {
                tree.Clear();
                tree.Clear();
            }
            catch (Exception ex)
            {
                Console.Write("Error info:" + ex.Message);
            }
        }
    }
}
